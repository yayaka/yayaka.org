import React from 'react'
import Link from 'gatsby-link'

const IndexPage = () => (
  <div>
    <h1>Index</h1>
    <h2></h2>
    <h2>YAYAKA NETWORK</h2>
    <p>
      The YAYAKA NETWORK is the place to communicate without any concerns.
      It liberates all communicative subjects and their networks from their
      locations, particular websites and software, authoritarians, and so on.
    </p>
    <h2>External Links</h2>
    <ul>
      <li>
        <a href="https://gitlab.com/yayaka">
          Our GitLab Group
        </a>
      </li>
    </ul>
  </div>
)

export default IndexPage
